from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Friend
from .api_csui_helper.csui_helper import CSUIhelper
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import os

response = {}
csui_helper = CSUIhelper()

def index(request):
    mahasiswa_list = csui_helper.instance.get_mahasiswa_list()
    page = request.GET.get('page', 1)

    paginator = Paginator(mahasiswa_list, 30)
    try:
        mahasiswa_page = paginator.page(page)
    except PageNotAnInteger:
        mahasiswa_page = paginator.page(1)
    except EmptyPage:
        mahasiswa_page = paginator.page(paginator.num_pages)

    # response['mahasiswa_list'] = mahasiswa_list
    friend_list = Friend.objects.all()
    html = 'lab_7/lab_7.html'
    response['mahasiswa_page'] = mahasiswa_page
    response['friend_list'] = friend_list
    return render(request, html, response)
# { 'mahasiswa_page': mahasiswa_page }

def friend_list(request):
    friend_list = Friend.objects.all()
    response['friend_list'] = friend_list
    html = 'lab_7/daftar_teman.html'
    return render(request, html, response)

def friend_list_json(request): # update
    friends = [obj.as_dict() for obj in Friend.objects.all()]
    return JsonResponse({"results": friends}, content_type='application/json')

@csrf_exempt
def add_friend(request):
    if request.method == 'POST':
        npm = request.POST['npm']
        name = request.POST['name']
        friend = Friend(friend_name=name, npm=npm)
        friend.save()
        return JsonResponse(friend.as_dict())

@csrf_exempt
def delete_friend(request, friend_id):
    if request.method == 'POST':
        Friend.objects.filter(id=friend_id).delete()
        data = {'id': friend_id}
        return JsonResponse(data)

@csrf_exempt
def validate_npm(request):
    npm = request.POST.get('npm', None)
    data = {
        'is_taken': Friend.objects.filter(npm=npm).exists()
    }
    return JsonResponse(data)
