## Checklist

### Mandatory
1. Membuat halaman untuk menampilkan semua mahasiswa fasilkom
    1. [x] Terdapat list yang berisi daftar mahasiswa fasilkom, yang dipanggil dari django model.
    2. [x] Buatlah tombol untuk dapat menambahkan list mahasiswa kedalam daftar teman (implementasikan menggunakan ajax).
    3. [x] Mengimplentasikan validate_npm untuk mengecek apakah teman yang ingin dimasukkan sudah ada didalam daftar teman atau belum.
    4. [x] Membuat pagination (hint: salah satu data yang didapat dari kembalian api.cs.ui.ac.id adalah `next` dan `previous` yang bisa digunakan dalam membuat pagination)
2. Membuat halaman untuk menampilkan daftar teman
    1. [x] Terdapat list yang berisi daftar teman, data daftar teman didapat menggunakan ajax.
    2. [ ] Buatlah tombol untuk dapat menghapus teman dari daftar teman (implementasikan menggunakan ajax).

    ---

    _Button manggil function addFriend(nama, npm)_  
    <span class="btn btn-xs btn-default" onClick="addFriend('{{ mahasiswa.nama }}', '{{ mahasiswa.npm }}')">  

    _Ini function addFriend(nama, npm) nya_  
    var addFriend = function(nama, npm) {  
        $.ajax({  
            **jadi awalnya dia bakal post lewat url itu dengan data itu**  
            method: "POST",  
            url: "{% url "lab-7:add-friend" %}",  
            data: { name: nama, npm: npm},  
            **terus otomatis function yg di views add_friend bakal kepanggil**  
            **dan bakal return response dalam bentuk friend.as_dict()**  
            success : function (friend) { **disini ditulis responsenya 'friend'**  
                html = '<a class="list-group-item clearfix">' +  
                    friend.friend_name + ' (' + friend.npm + ')' +  
                    '</a>';  
                $("#friend-list").append(html) **append di html element dengan id friend-list**
            },  
            error : function (error) {  
                alert("Mahasiswa dengan npm seperti ini sudah ada")  
            }  
        });  
    };  

    ---

3. Pastikan kalian memiliki _Code Coverage_ yang baik
    1. [ ] Jika kalian belum melakukan konfigurasi untuk menampilkan _Code Coverage_ di Gitlab maka lihat langkah `Show Code Coverage in Gitlab` di [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md)
    2. [ ] Pastikan _Code Coverage_ kalian 100%


### Additional

1. Membuat halaman yang menampilkan data lengkap teman
    1. [ ] Halaman dibuka setiap kali user mengklik salah satu teman pada halaman yang menampilkan daftar teman
    2. [ ] Tambahkan google maps yang menampilkan alamat teman pada halaman informasi detail (hint: https://developers.google.com/maps/documentation/javascript/)
1. Berkas ".env" untuk menyimpan username dan password, dapat menyebabkan akun anda terbuka untuk orang yang memiliki
   akses ke repository bila berkas tersebut ter-push ke repository.
   Hal ini sangat tidak baik dan bisa memalukan karena dapat membuka rahasia/privacy anda sendiri.
    1. [ ] Pastikan kerahasiaan dan privacy anda. Ubah mekanisme penyimpanan dan pengambilan bila diperlukan.
