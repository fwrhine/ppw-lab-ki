## Checklist

### Mandatory
1.  Session: Login & Logout
    1. [x] Implement Login function
    2. [x] Implement Logout function

2.  Session: Favourites
    1. [x] Implement function “Favoritkan” for Drones
    2. [x] Implement function “Hapus dari favorit” for Drones
    3. [x] Implement function “Reset favorit” for Drones

3. Cookies: Login & Logout
    1. [ ] Implement Login function
    2. [ ] Implement Logout function


4. Implement Header and Footer
    1. [ ] Make header function with logout button only if user has logged in (either on session or on cookies). Make it as good and interesting as possible.

5. Make sure you have a good _Code Coverage_
    1. [ ]  If you have not done the configuration to show _Code Coverage_ in Gitlab, you must configure it in `Show Code Coverage in Gitlab` on [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md)
    2. [ ] Make sure your _Code Coverage_ is 100%.



### Challenge
1. API Optical and SoundCard Implementation
    1. [ ] Add link to the Optical and SoundCard tab on Session Profile page
    2. [ ] Create table contains of data optical/soundcard

2.  Implement general function already provided to manage session :
    1. [ ] Use general function to add (add favorite) optical/soundcard to session
    2. [ ] Use gerenal funcion to delete (delete from favorite) optical/soundcard from session
    3. [ ] Use gerenal function to delete/reset category (drones/optical/soundcard) from session

3. Implement session for all page made on the previous labs
    1. [ ] If the lab page can be accessed without login first, then the login page will be shown
    2. [ ] If the lab-N page can be accessed without login first, the after login, lab-N page will be shown
    3. [ ] Change csui_helper.py implementation on Lab 9 so it can be used for Lab 7 (You can delete csui_helper.py on the Lab 7)
