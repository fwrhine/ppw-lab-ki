## Checklist

1.  Make Chat Box Page
    1. [x] Add  _lab_6.html_ into templates folder
    2. [x] Add _lab_6.css_ into _./static/css_
    3. [x] Add _lab_6.js_ file into _lab_6/static/js_ folder
    4. [x] Complete the _lab_6.js_ file so it can be run

2. Implement Calculator
    1. [x] Add section of code into  _lab_6.html_ file on templates folder
    2. [x] Add section of code into  _lab_6.css_ file on _./static/css_ folder
    3. [x] Add section of code into _lab_6.js_ file on _lab_6/static/js_ folder
    4. [x] Implement `AC` feature

3.  Implement select2
    1. [x] Load theme default match with selectedTheme
    2. [x] Populate data themes from local storage to select2
    3. [x] Local storage contains themes and selectedTheme
    4. [x] Color can change whenever we select selectedTheme

4.  Make sure you have a good _Code Coverage_
    1. [x]  If you have not done the configuration to show _Code Coverage_ in Gitlab, you must configure it in `Show Code Coverage in Gitlab` on [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md)
    2. [ ] Make sure your _Code Coverage_ is 100%.

###  Challenge Checklist
1. Qunit exercise
    1. [x] Implement from Qunit exercise
1. Chose one of these exercise you have to complete:
    1. Implement enter button on chat box
        1. [ ] Make a _Unit Test_ using Qunit
        2. [ ] Make a function that make the _Unit Test_ is _passed_
    1. Implement  `sin`, `log`, and `tan` function (HTML file is available)
        1. [x] Make a _Unit Test_ using Qunit
        2. [x] Maka a function that make the _Unit Test_ is _passed_
