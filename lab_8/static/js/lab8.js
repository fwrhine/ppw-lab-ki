window.fbAsyncInit = () => {
  FB.init({
    appId      : '1551718531610932',
    cookie     : true,
    xfbml      : true,
    version    : 'v2.11'
  });

  // implement a function that check login status (getLoginStatus)
  // and run render function below, with a boolean true as a paramater if
  // login status has been connected
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
        render(true);
    }
    else{
        render(false);
    }
  });


  // This is done because when the user opened the web and the user has been logged in,
  // it will automatically display the logged in view
};

// Facebook call init. default from facebook
(function(d, s, id){
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {return;}
  js = d.createElement(s); js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Render function, receive a loginFlag paramater that decide whether
// render or create a html view for the logged in user or not
// Modify this method as needed if you feel you need to change the style using
// Bootstrap's classes or your own implemented CSS
var render = loginFlag => {
  if (loginFlag) {
    // If the logged in view the one that will be rendered

    // Call getUserData method (see below) that have been implemented by you with a callback function
    // that receive user object as a parameter.
    // This user object is the response from an API facebook call.
    getUserData(user => {
      // Render profile view, post input form, post status button, and logout button
      $(".login").hide();
      $("#logout").show();
      $('#lab8').html(
        '<div class="profile">' +
        // '<div class="cover" src="' + user.cover.source + '" alt="cover" />' +
        '<img class="picture" src="' + user.picture.data.url + '" alt="profpic" />' +
        '<div class="data">' +
        '<h3>' + user.name + '</h3>' +
        '<p>' + user.email + '</p>' +
        '<p class="capitalize">' + user.gender + '</p>' +
        '</div>' +
        '</div>' +
        '<input id="postInput" type="text" class="post" placeholder="Ketik Status Anda" />' +
        '<button class="postStatus" onclick="postStatus()">Post to Facebook</button>' +
        '<br>' +
        '<br>' +
        '<br>' +
        '<h1> Feed </h1>'
      );

      $('#logout').html('<a href="#" onclick="facebookLogout()">Logout</a>');

      // After renderin the above view, get home feed data from the logged in account
      // by calling getUserFeed method which you implement yourself.
      // That method has to receive a callback parameter, which receive a feed object as a response
      // from calling the Facebook API
      getUserFeed(feed => {
        feed.data.map(value => {
          // Render the feed, customize as needed.
          if (value.message && value.story) {
            $('#lab8').append(
              '<div class="feed">' +
              '<p>' + value.message + '</p>' +
              '<p>' + value.story + '</p>' +
              '</div>'
            );
          } else if (value.message) {
            $('#lab8').append(
              '<div class="feed">' +
              '<p>' + value.message + '</p>' +
              '</div>'
            );
          } else if (value.story) {
            $('#lab8').append(
              '<div class="feed">' +
              '<p>' + value.story + '</p>' +
              '</div>'
            );
          }
        });
      });
    });
  } else {
    // The view when not logged in yet
    $('.login').show();
    $('.login').html(
        '<img class="facebook-image" src="https://facebookbrand.com/wp-content/themes/fb-branding/prj-fb-branding/assets/images/fb-art.png">' +
        '<button class="login-button" onclick="facebookLogin()">Login with Facebook</button>'
    );

    $('#lab8').html('');
    $("#logout").hide();
  }
};

var facebookLogin = () => {
  // TODO: Implement this method
  // Make sure this method receive a callback function that will call the render function
  // that will render logged in view after successfully logged in,
  // and this function has all needed permissions at the above scope.
  // You can modify facebookLogin function above
  FB.login(function(response){
    console.log(response);
    render(true);
}, {scope:'email, public_profile, user_posts, user_about_me, publish_actions, publish_pages'})
};

var facebookLogout = () => {
  // TODO: Implement this method
  // Make sure this method receive a callback function that will call the render function
  // that will render not logged in view after successfully logged out.
  // You can modify facebookLogout function above
  FB.getLoginStatus(function(response) {
   if (response.status === 'connected') {
     FB.logout();
     render(false);
   }
  });
};

// TODO: Complete this method
// This method modify the above getUserData method that receive a callback function called fun
// and the request user data from the logged in account with all the needed fields in render method,
// and call that callback function after doing the request and forward the response to that callback function.
// What does it mean by a callback function?
var getUserData = (fun) => {
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.api('/me?fields=id,cover,name,gender,picture.width(150).height(150),about,email', 'GET', function(response){
        console.log(response);
        fun(response);
      });
    }
  });
};

var getUserFeed = (fun) => {
  // TODO: Implement this method
  // Make sure this method receive a callback function parameter and do a data request to Home Feed from
  // the logged in account with all the needed fields in render method, and call that callback function
  // after doing the request and forward the response to that callback function.
  FB.getLoginStatus(function(response) {
    if (response.status === 'connected') {
      FB.api('/me/feed', 'GET', function(response){
        if (response && !response.error) {
          console.log(response);
          fun(response);
        }
      });
    }
  });
};

var postFeed = (message) => {
  // Todo: Implement this method,
  // Make sure this method receive a string message parameter and do a POST request to Feed
  // by going through Facebook API with a string message parameter as a message.
  FB.api('/me/feed', 'POST', {message:message});
  render(true);
};

var postStatus = () => {
  const message = $('#postInput').val();
  if(message == ""){
    alert("Status cannot be empty")
  } else{
    postFeed(message);
  }
};
