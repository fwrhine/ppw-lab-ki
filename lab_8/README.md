## Checklist

### Mandatory
1. Create a page for Login using OAuth
    1. [x] Register the application to facebook developer page
    2. [x] Do an OAuth Login using Facebook
    3. [x] Display user information from the logged in user using API
    4. [x] Do a facebook status post
    5. [x] Display post status at lab_8.html
    6. [x] Do a Logout
    7. [ ] Implement a beautiful and responsive css

2. Answer the questions from this document by writing it at your note book or source code that will be presented at demo

3. Make sure you have a good _Code Coverage_
    1. [ ] If you are not yet configure the _Code Coverage_ at Gitlab, then see the steps `Show Code Coverage in Gitlab` at [README.md](https://gitlab.com/PPW-2017/ppw-lab/blob/master/README.md)
    2. [ ] Make sure your _Code Coverage_ is 100%


### Additional

1. Do a delete status at facebook page
    1. [ ] Implement delete button at post status list
    2. [ ] Do a delete post status using existing Facebook API
